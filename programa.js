"use strict";
const viewHeight = 500;


// CONTRUCCION DE GRAFICAS 2D EN CANVAS:
function Graph2D(canvasElement){
    // REFERENCIAS NECESARIAS
    this.context = canvasElement.getContext("2d");
    // THIS. Toma LA REFERENCIA DE UN OBJETO DE ORIGEN

    // ESTABLECER LOS VALORES PREDETERMINADOS
    this.minX = -10; // VALOR MINIMO EN X
    this.maxX = 10; // VALOR MAXIMO EN X
    this.minY = -10; // VALOR MINIMO Y
    this.maxY = 10; // VALOR MINIMO EN Y
    this.baseColor = "rgb(50, 50, 50)"; //COLOR CUADRICULA
    this.xColor = "rgb(100, 25, 25)"; // EJE X COLOR EJE X 
    this.yColor = "rgb(25, 100, 25)"; // EJE Y COLOR EJE Y
    this.riemannColor1 = "rgb(0,100,255)";
    this.riemannColor2 = "rgb(0,255,100)";

    
    
    // Dibuja la cuadrícula de la ventana gráfica y borra cualquier contenido anterior.:
    this.drawGrid = function(){
        this.context.clearRect(0, 0, viewHeight * 2, viewHeight); //bORRA UN RECTANGULA DE LA SECCION DADA

        let ppuY = viewHeight / (this.maxY - this.minY); //DIBUJA LOS PIXELES POR CADA UNIDAD DE PANTALLA
        let ppuX = (viewHeight * 2) / (this.maxX - this.minX);

        let i;
        for(i = Math.ceil(this.minX); i <= this.maxX; i++){ //DIBUJAR LINEA HORIZONTAL DE IZQUIERDA A DERECHA; MATH.CEIL REDONDEA UN NUMERO
            if (i != 0){
                this.context.beginPath();
                this.context.moveTo(-(this.minX - i) * ppuX, 0);
                this.context.lineTo(-(this.minX - i) * ppuX, viewHeight)
                this.context.strokeStyle = this.baseColor;
                this.context.lineWidth = "2";
                this.context.stroke();
            }
        }
        for(i = Math.floor(this.maxY); i >= this.minY; i--){ //DIBUJAR UNA LINEA VERTICAL EN EL EJE Y.math.floor redondea el decimal al entero mayor de el numero dado
            if (i != 0){
                this.context.beginPath();
                this.context.moveTo(0, (this.maxY - i) * ppuY);
                this.context.lineTo(viewHeight * 2, (this.maxY - i) * ppuY)
                this.context.strokeStyle = this.baseColor;
                this.context.lineWidth = "2";
                this.context.stroke();
            }
        }
        // GRAFICA FUNCION SUPERIOR 
        if (this.minX <= 0){ //si el eje X está dentro de la ventana gráfica, dibuje
            this.context.beginPath();
            this.context.moveTo(-(this.minX) * ppuX, 0);
            this.context.lineTo(-(this.minX) * ppuX, viewHeight);
            this.context.strokeStyle = this.yColor;
            this.context.lineWidth = "4";
            this.context.stroke();
        }
        if (this.minY <= 0){ // if the Y axis is inside the viewport, draw it
            this.context.beginPath();
            this.context.moveTo(0, this.maxY * ppuY);
            this.context.lineTo(viewHeight * 2, this.maxY * ppuY);
            this.context.strokeStyle = this.xColor;
            this.context.lineWidth = "4";
            this.context.stroke();
        }
    };

    // RELLENO DE RECTANGULOS PARA UNA REGION ESPECIFICA
    this.fillRegion = function(x1, y1, x2, y2){
        let ppuX = (viewHeight * 2) / (this.maxX - this.minX); // PIXELES POR REGION 
        let ppuY = viewHeight / (this.maxY - this.minY);

        let width = ppuX * (x2 - x1); // ANCHO 
        let height = ppuY * (y2 - y1); // ALTO 
        let left = (x1 - this.minX) * ppuX; //BASE 
        let top = -(y2 - this.maxY) * ppuY; // ARRIBA 

        this.context.lineWidth = "1"; // (GROSOR;RELLENO,FORMA)
        this.context.beginPath();
        this.context.fillRect(left, top, width, height);
        this.context.stroke();
    };

    //DIBUJA UNA SEGUNDA PARABOLA 
    this.drawParabola = function(translateX, translateY, scaleX, scaleY){ // CAMBIAN LAS DIMENSIONES DE LA FUNCION
        let ppuX = (viewHeight * 2) / (this.maxX - this.minX); // PIXELES POR UNIDAD EN LA PANTALLA
        let ppuY = viewHeight / (this.maxY - this.minY);
        let unitStep = (this.maxX - this.minX) / (viewHeight * 2); // PIXELES POR UNIDAD 
        
        this.context.strokeStyle = this.funcColor;
        this.context.moveTo(0, -(Math.pow(this.minX, 2) - this.maxY) * ppuY);// MATH.POW ELEVA LA EXPRESION DE UNA BASE
        this.context.beginPath();

        let currentStep = this.minX;
        while(currentStep <= this.maxX){ //iterar a lo largo del eje x, un píxel a la vez para dibujar la función continua
            let nextX = (currentStep - this.minX) * ppuX;
            let nextY = (this.maxY - ((Math.pow((1/scaleX) * (currentStep - translateX), 2) * scaleY) + translateY)) * ppuY;

            this.context.lineTo(nextX, nextY);
            currentStep += unitStep; //INTERVALO DE PIXELES PARA LA FUNCION 
        }
        this.context.stroke();
    };
    //DIBUJO DE LA SUMATORIA DE RIEMANN
    this.drawRiemannSumParabola = function(translateX, translateY, scaleX, scaleY, partitions, XrangeMin, XrangeMax, entireOutput){
        let partitionStep = (XrangeMax - XrangeMin) / (partitions + 1); //unidades por región de partición

        let areaCalculation = "";
        let totalArea = 0;

        let currentStep = XrangeMin;
        let flipFlop = false;
        let i;
        for(i = 0; i <= partitions; i++){ // draw each partition
            let partitionY = ((Math.pow((1/scaleX) * (currentStep - translateX), 2) * scaleY) + translateY);

            flipFlop = !flipFlop; //ALTERNAR LOS VALORES PARA COMBINAR LOS COLORES EN LAS PARTICIONES
            if (flipFlop){
                this.context.fillStyle = this.riemannColor1;
            }else{
                this.context.fillStyle = this.riemannColor2;
            }
            this.fillRegion(currentStep, 0, currentStep + partitionStep, partitionY);

            totalArea += partitionY; // Actualizar la informacion 
            if (entireOutput){
                areaCalculation += " + (" + partitionStep.toFixed(4) + " * " + partitionY.toFixed(4) + ")";
            }

            currentStep += partitionStep; //SIGUIENTE PARTICION 
        }

        totalArea = totalArea * partitionStep; //multiplica la suma final por el ancho de la partición
        areaCalculation = totalArea.toFixed(8) + " = " + areaCalculation;

        if(entireOutput){ //devolver una salida basada en lo solicitado
            return areaCalculation;
        }
        else{
            return totalArea.toFixed(8);
        }
    };

    // Dibuja medio círculo en el lado positivo del eje x:
    this.drawSemiCircle = function(radius){
        let ppuX = (viewHeight * 2) / (this.maxX - this.minX); // PIXELES POR UNIDAD EN PANTALLA
        let ppuY = viewHeight / (this.maxY - this.minY);
        let unitStep = (this.maxX - this.minX) / (viewHeight * 2); //PIXELES POR UNIDAD EN PANTALLA 

        this.context.strokeStyle = this.funcColor;
        this.context.moveTo((-this.minX - radius) * ppuX, this.maxY * ppuY);
        this.context.beginPath();

        let currentStep = (-radius) // comenzar en el cuadrante izquierdo del círculo
        while(currentStep <= radius){ // dibuje iterativamente la función paso a paso de un píxel a la vez
            let nextX = (currentStep - this.minX) * ppuX;
            let nextY = (this.maxY - Math.sqrt(radius * radius - currentStep * currentStep)) * ppuY;
            this.context.lineTo(nextX, nextY);

            currentStep += unitStep; // step one pixel along the x-axis
        }
        this.context.stroke();
    };
    // Draws riemann regions for the circle function:
    this.drawRiemannSumSemiCircle = function(radius, partitions){
        let partitionStep = (2 * radius) / (partitions + 1); // units per partition region

        let totalArea = 0;
        let currentStep = -radius;
        let flipFlop = false;
        let i;
        for(i = 0; i <= partitions; i++){ // draw the partition regions from left to right
            let partitionY = Math.sqrt(radius * radius - currentStep * currentStep);

            flipFlop = !flipFlop; // alternate region colors
            if (flipFlop){
                this.context.fillStyle = this.riemannColor1;
            }else{
                this.context.fillStyle = this.riemannColor2;
            }
            this.fillRegion(currentStep, 0, currentStep + partitionStep, partitionY);

            totalArea += partitionY;
            currentStep += partitionStep;
        }

        totalArea = totalArea * partitionStep; // calculate and return total area(full string not implemented)
        return totalArea.toFixed(8);
    };

    // Draws the untransformed sine wave:
    this.drawSinWave = function(){
        let ppuX = (viewHeight * 2) / (this.maxX - this.minX); // pixels per unit
        let ppuY = viewHeight / (this.maxY - this.minY);
        let unitStep = (this.maxX - this.minX) / (viewHeight * 2); // units per pixel
        
        this.context.strokeStyle = this.funcColor;
        this.context.moveTo(0, (this.maxY - Math.sin(this.minX)) * ppuY);
        this.context.beginPath();

        let currentStep = this.minX;
        while(currentStep <= this.maxX){ // iterate along the x axis, one pixel at a time to draw the continuous function
            let nextX = (currentStep - this.minX) * ppuX;
            let nextY = (this.maxY - Math.sin(currentStep)) * ppuY;

            this.context.lineTo(nextX, nextY);
            currentStep += unitStep; // step one pixel along the x-axis
        }
        this.context.stroke();
    };
    // Draws reimann regions for the sine wave:
    this.drawRiemannSumSinWave = function(partitions, XrangeMin, XrangeMax){
        let partitionStep = (XrangeMax - XrangeMin) / (partitions + 1); // units per partition region

        let totalArea = 0;
        let currentStep = XrangeMin;
        let flipFlop = false;
        let i;
        for(i = 0; i <= partitions; i++){ // draw each partition
            let partitionY = Math.sin(currentStep);

            flipFlop = !flipFlop; // alternate colors of partitions
            if (flipFlop){
                this.context.fillStyle = this.riemannColor1;
            }else{
                this.context.fillStyle = this.riemannColor2;
            }
            this.fillRegion(currentStep, 0, currentStep + partitionStep, partitionY);

            totalArea += partitionY;
            currentStep += partitionStep; // step to the next partition
        }

        totalArea = totalArea * partitionStep; // multiple the final sum by the partition width
        return totalArea.toFixed(8);
    };
};

//
// Riemann Sum Interaction Logic
//

// Allow the page vertical scrolling to be locked/unlocked:
var scrollFrozen = false;
var frozenPosition = 0;
// Bind this to the window scroll event:
function onScroll(){
    if (scrollFrozen){
        window.scrollTo(0, frozenPosition);
    }
};
// Call once when the scrolling should be frozen:
function freezeScroll(){
    frozenPosition = window.scrollY;
    scrollFrozen = true;
};
// Call once when the scrolling should be unfrozen:
function unfreezeScroll(){
    scrollFrozen = false;
};

// Bind the first slider to update the contents of graph B:
var sumSlider = document.querySelector("#sum-slider");
var sumOut = document.querySelector("#sum-output");
sumSlider.oninput = function(){
    graphB.drawGrid();
    sumOut.innerHTML = graphB.drawRiemannSumParabola(0,4,2,-1, Math.round(this.value), -4 ,4, true);
    graphB.drawParabola(0,4,2,-1);
};

// Bind the exit page button:
document.querySelector("#anti-activity-button").addEventListener("click", () => {
    window.close();
});

// Define logic for the activity:
var activityOpen = false;
var currentGraphFunction = 1;
var currentPartitions = 1;
var background = document.querySelector("#activity-bg"); // GRAFICA 1
var activityGraph = new Graph2D(document.querySelector("#output-graph3")); //grafica 3
var activityInfo = document.querySelector("#activity-info"); //GRAFICA 2 
// Bind button to open the modal activity:
document.querySelector("#activity-button").addEventListener("click", () => {
    if (!activityOpen){
        background.classList.remove("activity-hidden");
        activityOpen = true;
        freezeScroll();
    }
});
//GRAFICO DE LA SUMATORIA
function redrawActivity(){
    switch(currentGraphFunction){ // BIDUJO BASADO EN CURRENT.GRAPH
        case 1:
            activityGraph.minX = -6;
            activityGraph.maxX = 6;
            activityGraph.minY = -1;
            activityGraph.maxY = 5;
            activityGraph.drawGrid();
            let areaA = activityGraph.drawRiemannSumParabola(0,4,2,-1, currentPartitions, -4 ,4, false);
            activityInfo.innerHTML = "Function: Parabola | Partitions: " + currentPartitions + " | Riemann Area: " + areaA + " | True Area: (64/3) = 21.33333333";
            activityGraph.drawParabola(0,4,2,-1);
            break;
        case 2:
            activityGraph.minX = -3;
            activityGraph.maxX = 3;
            activityGraph.minY = -1;
            activityGraph.maxY = 3;
            activityGraph.drawGrid();
            let areaB = activityGraph.drawRiemannSumSemiCircle(2, currentPartitions);
            activityInfo.innerHTML = "Function: Circle | Partitions: " + currentPartitions + " | Riemann Area: " + areaB + " | True Area: (2pi) = 6.28318531";
            activityGraph.drawSemiCircle(2);
            break;
        case 3:
            activityGraph.minX = -1;
            activityGraph.maxX = 6;
            activityGraph.minY = -2;
            activityGraph.maxY = 2;
            activityGraph.drawGrid();
            let areaC = activityGraph.drawRiemannSumSinWave(currentPartitions, 0, Math.PI);
            activityInfo.innerHTML = "Function: Sin | Partitions: " + currentPartitions + " | Riemann Area: " + areaC + " | True Area: 2";
            activityGraph.drawSinWave();
            break;
    }
};
// Handle keystroke input for the activity:
document.body.addEventListener("keydown", (e) => {
    if (activityOpen){ // ignore keystrokes when activity is closed
        switch(e.keyCode){
            case 27: // esc
                background.classList.add("activity-hidden");
                activityOpen = false;
                unfreezeScroll();
                break;
            case 37: // left
                if (currentGraphFunction > 1){
                    currentGraphFunction--;
                    redrawActivity();
                }
                break;
            case 39: // right
                if (currentGraphFunction < 3){
                    currentGraphFunction++;
                    redrawActivity();
                }
                break;
            case 38: // up
                if (currentPartitions < 1000){
                    currentPartitions++;
                    redrawActivity();
                }
                break;
            case 40: // down
                if (currentPartitions > 1){
                    currentPartitions--;
                    redrawActivity();
                }
                break;
        }
    }
});

//
// Initialize Page Objects
//

var graphA = new Graph2D(document.querySelector("#output-graph"));
graphA.minX = -6;
graphA.maxX = 6;
graphA.minY = -1;
graphA.maxY = 5;
graphA.drawGrid();
graphA.drawParabola(0,4,2,-1);

var graphB = new Graph2D(document.querySelector("#output-graph2"));
graphB.minX = -6;
graphB.maxX = 6;
graphB.minY = -1;
graphB.maxY = 5;
graphB.drawGrid();
graphB.drawRiemannSumParabola(0,4,2,-1, 9, -4 ,4, false);
graphB.drawParabola(0,4,2,-1);

window.addEventListener("scroll", onScroll);

redrawActivity();